use std::path::{Path, PathBuf};
use std::error::Error;
use std::result::Result;
use std::os::unix::net::{UnixStream, UnixListener};
use std::os::unix::io::AsRawFd;
use std::io::{Read, Write};
use std::ffi::OsString;
use std::os::unix::ffi::OsStringExt;
use std::fs::{remove_file, remove_dir, rename, read_dir};
use nix::unistd::{
	fork, ForkResult, setuid, Uid, getpid, mkdir, access, AccessFlags};
use nix::fcntl::{open, OFlag};
use nix::sys::stat::{Mode, umask};
use nix::sys::uio::IoVec;
use nix::sys::socket::{ControlMessage, MsgFlags, sendmsg};
use byteorder::{ReadBytesExt, WriteBytesExt, LittleEndian};

// All numbers are little-endian
//
// Message header:
// - u8 code (see MessageCode)
//
// INIT message - will setuid() the daemon instance:
// - u16 - secret key length
// - [char] - secret key
// - u32 - UID to setuid into
// Response:
// - u8 0/1 - success
//
// STOP message - will stop the daemon instance:
// empty
// Response:
// - u8 1 - success (can't fail)
// PING message - quick connection test:
// empty
// Response:
// - u8 1 - success (can't fail)
//
// OPEN message - will call open():
// - u16 - path length
// - [char] - path
// - i32 - open() flags
// - u32 - open() mode
// Response:
// - u8 0/1 - success
// - i32 - open() return value on failure, zero otherwise
// on success, the fd will be sent in the first anciliary control message
//
// Ideally we'd make do with *at() functions, but a lot of languages don't
// have bindings for those
//
// MKDIR message - will call mkdir():
// - u16 - path length
// - [char] - path
// - u32 - mkdir() mode
// Response:
// - u8 0/1 - success
// - i32 - mkdir() return value on failure, zero otherwise
//
// UNLINK message - will call unlink():
// - u16 - path length
// - [char] - path
// Response:
// - u8 0/1 - success
// - i32 - unlink() return value on failure, zero otherwise
//
// RMDIR message - will call rmdir():
// - u16 - path length
// - [char] - path
// Response:
// - u8 0/1 - success
// - i32 - rmdir() return value on failure, zero otherwise
//
// RENAME message - will call rename():
// - u16 - source path length
// - [char] - source path
// - u16 - destination path length
// - [char] - destination path
// Response:
// - u8 0/1 - success
// - i32 - rename() return value on failure, zero otherwise
//
// ACCESS message - will call access():
// - u16 - path length
// - [char] - path
// - u8 - mode
// Response:
// - u8 0/1 - success
// - i32 - access() return value on failure, zero otherwise
//
// SCANDIR message - will return filenames of all files in a directory:
// - u16 - directory path length
// - [char] - directory path
// - u8 0/1 - return only regular files and directories/return all files
// Response:
// - u8 0/1 - success
// - i32 - access() return value on failure, zero otherwise
// On success:
//   For each file:
//   - u16 - filename length
//   - [char] - filename
//  - u16 0 - list terminator
//
// Needed because there's no way in PHP to call readdir() on a fd
//
// On receiving a malformed message, the daemon will send:
// - u8 2 - fatal error signifier
// - u16 - string error length
// - [char] - string error
// and close the connection

const MESSAGE_CODE_INIT: u8 = 1;
const MESSAGE_CODE_STOP: u8 = 2;
const MESSAGE_CODE_PING: u8 = 3;
const MESSAGE_CODE_OPEN: u8 = 4;
const MESSAGE_CODE_MKDIR: u8 = 5;
const MESSAGE_CODE_UNLINK: u8 = 6;
const MESSAGE_CODE_RMDIR: u8 = 7;
const MESSAGE_CODE_RENAME: u8 = 8;
const MESSAGE_CODE_ACCESS: u8 = 9;
const MESSAGE_CODE_SCANDIR: u8 = 10;

const RESPONSE_ERR: u8 = 0;
const RESPONSE_OK: u8 = 1;
const RESPONSE_FATAL_ERR: u8 = 2;

macro_rules! pid_dbg {
	($($arg:tt)*) => { println!("{}: {}", getpid(), format!($($arg)*)); }
}

fn read_u8_vec(stream: &mut UnixStream) -> Result<Vec<u8>, Box<dyn Error>> {
	let length = stream.read_u16::<LittleEndian>()?;
	let mut data = vec![0; length as usize];
	stream.read(&mut data)?;
	Ok(data)
}

fn read_path(stream: &mut UnixStream) -> Result<PathBuf, Box<dyn Error>> {
	let bytes = read_u8_vec(stream)?;
	let string = OsString::from_vec(bytes);
	Ok(PathBuf::from(string))
}

fn handle_io_result<T>(stream: &mut UnixStream, res: std::io::Result<T>)
		-> Result<Option<T>, Box<dyn Error>> {
	match res {
		Ok(res) => {
			stream.write_u8(RESPONSE_OK)?;
			stream.write_i32::<LittleEndian>(0)?;
			Ok(Some(res))
		}
		Err(e) => {
			stream.write_u8(RESPONSE_ERR)?;
			let errno = e.raw_os_error().ok_or("Failed to get errno")?;
			pid_dbg!("    ERR {}", errno);
			stream.write_i32::<LittleEndian>(errno as i32)?;
			Ok(None)
		}
	}
}

fn handle_nix_result(stream: &mut UnixStream, res: nix::Result<()>)
		-> Result<(), Box<dyn Error>> {
	match res {
		Ok(()) => {
			stream.write_u8(RESPONSE_OK)?;
			stream.write_i32::<LittleEndian>(0)?;
		}
		Err(e) => {
			stream.write_u8(RESPONSE_ERR)?;
			let errno = e.as_errno().ok_or("Failed to get errno")?;
			pid_dbg!("    ERR {}", errno);
			stream.write_i32::<LittleEndian>(errno as i32)?;
		}
	}

	Ok(())
}

// Returns whether to close the connection
fn process_message(secret_key: &str,
				   stream: &mut UnixStream,
				   is_initialized: &mut bool)
		-> Result<bool, Box<dyn Error>> {
	let msg_code = stream.read_u8()?;
	//pid_dbg!("Reading message with code {}", msg_code);

	match msg_code {
		MESSAGE_CODE_INIT | MESSAGE_CODE_STOP | MESSAGE_CODE_PING => (),
		_ if !*is_initialized =>
			return Err("Connection not successfully initialized".into()),
		_ => (),
	}

	match msg_code {
		MESSAGE_CODE_INIT => {
			let given_key_bytes = read_u8_vec(stream)?;
			let given_key = String::from_utf8(given_key_bytes)?;
			let uid = stream.read_u32::<LittleEndian>()?;

			// TODO: Constant-time comparison
			if given_key != secret_key {
				stream.write_u8(RESPONSE_ERR)?;
			} else {
				setuid(Uid::from_raw(uid)).map_err(|e| format!("Failed setuid({}): {}", uid, e))?;
				stream.write_u8(RESPONSE_OK)?;
			}
			stream.flush()?;

			*is_initialized = true;

			Ok(false)
		}
		MESSAGE_CODE_STOP => {
			stream.write_u8(RESPONSE_OK)?;
			Ok(true)
		}
		MESSAGE_CODE_PING => {
			stream.write_u8(RESPONSE_OK)?;
			Ok(false)
		}
		MESSAGE_CODE_OPEN => {
			let path = read_u8_vec(stream)?;
			let flags_int = stream.read_i32::<LittleEndian>()?;
			let flags = OFlag::from_bits(flags_int).ok_or("Failed to parse flags")?;
			let mode_int = stream.read_u32::<LittleEndian>()?;
			let mode = Mode::from_bits(mode_int).ok_or("Failed to parse mode")?;

			pid_dbg!("open({:?}, {:?})", String::from_utf8(path.clone())?, flags);
			match open(&path[..], flags, mode) {
				Ok(fd) => {
					// We can't use the socket directly via Write, because we
					// need to send the fd
					let mut resp_data = vec![];
					resp_data.write_u8(RESPONSE_OK)?;
					resp_data.write_i32::<LittleEndian>(0)?;

					// Based on test_scm_rights() in
					// https://github.com/jonas-schievink/nix/blob/master/test/sys/test_socket.rs
					let iov = [IoVec::from_slice(&resp_data[..])];
					let fds = [fd];
					let cmsg = ControlMessage::ScmRights(&fds);
					sendmsg(stream.as_raw_fd(), &iov, &[cmsg], MsgFlags::empty(), None)?;
				}
				Err(e) => {
					stream.write_u8(RESPONSE_ERR)?;
					let errno = e.as_errno().ok_or("Failed to get errno")?;
					pid_dbg!("    ERR {}", errno);
					stream.write_i32::<LittleEndian>(errno as i32)?;
				}
			}

			Ok(false)
		}
		MESSAGE_CODE_MKDIR => {
			let path = read_u8_vec(stream)?;
			let mode_int = stream.read_u32::<LittleEndian>()?;
			let mode = Mode::from_bits(mode_int).ok_or("Failed to parse mode")?;

			handle_nix_result(stream, mkdir(&path[..], mode))?;

			Ok(false)
		}
		MESSAGE_CODE_UNLINK => {
			let path = read_path(stream)?;
			handle_io_result(stream, remove_file(path))?;
			Ok(false)
		}
		MESSAGE_CODE_RMDIR => {
			let path = read_path(stream)?;
			handle_io_result(stream, remove_dir(path))?;
			Ok(false)
		}
		MESSAGE_CODE_RENAME => {
			let path_from = read_path(stream)?;
			let path_to = read_path(stream)?;
			handle_io_result(stream, rename(path_from, path_to))?;
			Ok(false)
		}
		MESSAGE_CODE_ACCESS => {
			let path = read_u8_vec(stream)?;
			let mode = AccessFlags::from_bits(stream.read_u8()? as i32)
				.ok_or("Failed to parse mode")?;
			pid_dbg!("access({:?}, {:?})", String::from_utf8(path.clone())?, mode);
			handle_nix_result(stream, access(&path[..], mode))?;
			Ok(false)
		}
		MESSAGE_CODE_SCANDIR => {
			let path = read_path(stream)?;
			let return_all = stream.read_u8()?;
			pid_dbg!("scandir for {:?}", path);
			if let Some(entries) = handle_io_result(stream, read_dir(path))? {
				for entry in entries {
					let entry = entry?;
					let file_type = entry.file_type()?;
					if (return_all == 0) &&
					   !(file_type.is_file() || file_type.is_dir()) {
						continue;
					}
					let filename = entry.file_name();
					pid_dbg!("  - {:?}", filename);
					stream.write_u16::<LittleEndian>(filename.len() as u16)?;
					stream.write(&filename.into_vec())?;
				}
				stream.write_u16::<LittleEndian>(0)?;
			}
			Ok(false)
		}
		_ => {
			Err("Invalid message code".into())
		}
	}
}

fn child_process(secret_key: String, mut stream: UnixStream) {
	pid_dbg!("Accepted connection!");
	let mut is_initialized = false;
	loop {
		match process_message(&secret_key, &mut stream, &mut is_initialized) {
			Ok(false) => (),
			Ok(true) => break,
			Err(e) => {
				pid_dbg!("Fatal error! {:?}", e);
				// We'll ignore errors at this point, the client might have
				// disconnected at any point (not that a well-behaved client should)
				let _ = stream.write_u8(RESPONSE_FATAL_ERR);
				let msg = e.to_string();
				let msg_bytes = msg.as_bytes();
				let _ = stream.write_u16::<LittleEndian>(msg_bytes.len() as u16);
				let _ = stream.write(msg_bytes);
				break
			},
		}
	}
	pid_dbg!("Exiting...");
}

fn main() {
	let args: Vec<String> = std::env::args().collect();
	if args.len() != 3 {
		eprintln!("Usage: {} <socket> <secret key>", args[0]);
		std::process::exit(1);
	}

	let socket_path = Path::new(&args[1]);
	let secret_key = &args[2];

	if socket_path.exists() {
		std::fs::remove_file(socket_path)
			.expect("Error deleting leftover socket");
	}

	umask(Mode::S_IRWXO);

	let listener = UnixListener::bind(socket_path)
		.expect("Error creating socket listener");
	for stream_result in listener.incoming() {
		let stream = stream_result.expect("Error receiving incoming stream");
		if let ForkResult::Child = fork().expect("Error forking") {
			child_process(secret_key.to_owned(), stream);
			return;
		}
	}
}
