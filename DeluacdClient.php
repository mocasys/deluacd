<?php
// XXX: This file doesn't implement all messages and is possibly outdated

class DeluacdException extends Exception { }

class DeluacdIoException extends Exception {
	function __construct(string $msg) {
		parent::__construct($msg . ": " . socket_strerror(socket_last_error()));
	}
}

class DeluacdClient {
	const MESSAGE_INIT = 1;
	const MESSAGE_STOP = 2;
	const MESSAGE_OPEN = 4;

	const RESPONSE_ERR = 0;
	const RESPONSE_OK = 1;
	const RESPONSE_FATAL_ERR = 2;

	private Socket $socket;


	function __construct(string $socketPath, string $secretKey, int $uid) {
		$this->socket = socket_create(AF_UNIX, SOCK_STREAM, 0);
		if(!$this->socket)
			throw new DeluacdIoException("Failure creating socket");

		if(!socket_connect($this->socket, $socketPath))
			throw new DeluacdIoException("Failure connecting");

		// Send INIT message
		$this->send(
			pack("Cv", self::MESSAGE_INIT, strlen($secretKey)) .
			$secretKey .
			pack("V", $uid));
		$outCode = $this->readCommonResp();
		if($outCode !== self::RESPONSE_OK) {
			throw new DeluacdException("Failed to initialize");
		}
	}

	function __destruct() {
		$this->send(pack("C", self::MESSAGE_STOP));
		$this->readCommonResp();
		socket_close($this->socket);
	}

	public function open(string $path, int $flags, int $creationMode) {
		$this->send(
			pack("Cv", self::MESSAGE_OPEN, strlen($path)) .
			$path .
			pack("VV", $flags, $creationMode));

		$recvData = [
			"name" => [],
			"buffer_size" => 2048,
			"controllen" => socket_cmsg_space(SOL_SOCKET, SCM_RIGHTS, 1)
		];
		if(!socket_recvmsg($this->socket, $recvData, 0))
			throw new DeluacdIoException("Unable to receive open response");

		$msg = $recvData["iov"][0];
		$outCode = unpack("C", $msg)[1];
		$errno = unpack("V", $msg, 1)[1];
		if($outCode === self::RESPONSE_OK) {
			$fd = $recvData["control"][0]["data"][0];
			return $fd;
		} else if($outCode === self::RESPONSE_ERR) {
			return -$errno;
		} else if($outCode == self::RESPONSE_FATAL_ERR) {
			$msg .= $this->read(4096); // Fetch the rest
			$errlen = unpack("v", $msg, 1)[1];
			$errmsg = substr($msg, 3, $errlen);
			throw new DeluacdException("Fatal error: " . $errmsg);
		}
	}

	private function send(string $msg) {
		if(socket_send($this->socket, $msg, strlen($msg), 0) !== strlen($msg))
			throw new DeluacdIoException("Failure sending message");
	}

	private function read(int $length) {
		$data = socket_read($this->socket, $length);
		if(!$data) {
			throw new DeluacdIoException("Failure reading from socket");
		}
		return $data;
	}

	private function readU16String() {
		$len = unpack("v", $this->read(2))[1];
		return $this->read($len);
	}

	private function readCommonResp() {
		$code = unpack("C", $this->read(1))[1];
		if($code == self::RESPONSE_FATAL_ERR) {
			throw new DeluacdException("Fatal eror: " . $this->readU16String());
		}
		return $code;
	}

}
